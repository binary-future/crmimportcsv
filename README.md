## Import transactions

### Filemap:
 - `database_create.sql` - stores SQL script for creating database
 - `queries.sql` - Four SQL queries which were described in task.php
 - `index.php` - entry point for import script. Change filepath to csv report file
  in that file if you need to import another file