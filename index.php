<?php
declare(strict_types=1);

use App\Utils\Config\Config;
use App\Utils\Config\Contracts\Config as ConfigInterface;
use App\Utils\CSVReader\Contracts\CSVReader as CSVReaderInterface;
use App\Utils\CSVReader\CSVReader;
use App\Utils\Database\Connection;
use App\Utils\Database\Connector;
use App\Utils\Database\Contracts\Builder as BuilderInterface;
use App\Utils\Database\Contracts\Connection as ConnectionInterface;
use App\Utils\Database\Contracts\Connector as ConnectorInterface;
use App\Utils\Database\Query\Builder;

require_once __DIR__ . '/vendor/autoload.php';

$containerBuilder = new DI\ContainerBuilder();

$containerBuilder->addDefinitions([
    'basePath' => __DIR__,
    ConfigInterface::class => DI\get(Config::class),
    CSVReaderInterface::class => DI\get(CSVReader::class),
    BuilderInterface::class => DI\get(Builder::class),
    ConnectorInterface::class => DI\get(Connector::class),
    ConnectionInterface::class => DI\get(Connection::class)
]);

$container = $containerBuilder->build();

$command = $container->make(\App\Commands\ImportTransactionsFromCSV::class);
$command->execute(__DIR__ . '/data/report.csv');