# - display all transactions for a batch (merchant + date + ref num)
#       date, type, card_type, card_number, amount

SELECT
    trans.date,
    trans.type,
    cds.type          AS card_type,
    trans.card_number AS card_number,
    trans.amount
FROM transactions AS trans
         INNER JOIN cards AS cds ON cds.id = trans.card_id
         INNER JOIN batches AS b ON b.id = trans.batch_id
    AND b.date = '2018-05-05'
    AND b.reference_number = '229660187232128093216672'
         INNER JOIN merchants AS m ON m.id = b.merchant_id
    AND m.merchant_id = '767742410631437882';


# - display stats for a batch
#   per card type (VI - 2 transactions with $100 total, MC - 10 transaction with $200 total)
SELECT
    b.date,
    b.reference_number,
    crd.type AS card_type,
    COUNT(trans.amount) AS transactions,
    SUM(trans.amount) AS amount
FROM transactions AS trans
         INNER JOIN cards AS crd ON crd.id = trans.card_id
         INNER JOIN batches AS b ON b.id = trans.batch_id
    AND b.date = '2018-05-05'
    AND b.reference_number = '767742410631437882'
GROUP BY
    b.date, b.reference_number, crd.type
ORDER BY
    b.date, b.reference_number;

# - display stats for a merchant and a given date range

SELECT
    m.id           AS merchant_id,
    m.name           AS merchant_name,
    crd.type        AS card_type,
    COUNT(trans.amount) AS transactions,
    SUM(trans.amount)   AS amount
FROM merchants AS m
         INNER JOIN batches AS b ON b.merchant_id = m.id
         INNER JOIN transactions AS trans ON trans.batch_id = b.id
         INNER JOIN cards AS crd ON crd.id = trans.card_id

WHERE trans.date BETWEEN '2018-05-04' AND '2018-05-05'
  AND m.merchant_id = '2264135688721936'
GROUP BY
    m.name, m.id, crd.type
ORDER BY
    m.id,
    m.name;


# - display top 10 merchants (by total amount) for a given date range
#       merchant id, merchant name, total amount, number of transactions

SELECT
    m.id AS merchant_id,
    m.name AS merchant_name,
    COUNT(t.amount) AS transactions,
    SUM(t.amount) AS amount
FROM transactions AS t
         INNER JOIN batches AS b ON b.id = t.batch_id
         INNER JOIN merchants AS m ON m.id = b.merchant_id
WHERE t.date BETWEEN '2018-05-04' AND '2018-05-05'
GROUP BY
    m.id,
    m.name
ORDER BY amount DESC
LIMIT 10;