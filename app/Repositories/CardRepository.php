<?php


namespace App\Repositories;


use App\Models\Card\Card;
use App\Utils\Database\Contracts\Connection;
use App\Utils\Database\Contracts\Builder;

final class CardRepository implements \App\Repositories\Contracts\CardRepository
{
    private const FIELD_ID = 'id';
    private const FIELD_TYPE = 'type';
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var Builder
     */
    private $builder;

    private $table = 'cards';

    /**
     * CardRepository constructor.
     * @param Connection $connection
     * @param Builder $builder
     */
    public function __construct(Connection $connection, Builder $builder)
    {
        $builder->table($this->table);

        $this->connection = $connection;
        $this->builder = $builder;
    }

    /**
     * @param string $type
     * @param int|null $id
     * @return Card
     */
    public function make(string $type, ?int $id = null): Card
    {
        return new Card($id, $type);
    }

    /**
     * @param string $type
     * @return Card
     * @throws \Exception
     */
    public function create(string $type): Card
    {
        $bindings = [
            self::FIELD_TYPE => $type
        ];
        $query = $this->builder->insert($bindings);

        if (! $this->connection->insert($query, $bindings)) {
            // TODO: create repository exception
            throw new \Exception('Card was not created');
        }
        $cardId = $this->connection->lastInsertId();

        return $this->make($type, $cardId);
    }


}