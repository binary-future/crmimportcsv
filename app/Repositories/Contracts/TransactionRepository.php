<?php

namespace App\Repositories\Contracts;

use App\Models\Batch\Batch;
use App\Models\Card\Card;
use App\Models\Transaction\Transaction;
use App\Models\Transaction\TransactionType;
use Carbon\Carbon;

interface TransactionRepository
{
    /**
     * @param Carbon $date
     * @param TransactionType $transactionType
     * @param Card $card
     * @param string $cardNumber
     * @param float $amount
     * @param Batch $batch
     * @param int $id
     * @return Transaction
     */
    public function make(Carbon $date, TransactionType $transactionType, Card $card, string $cardNumber, float $amount, Batch $batch, ?int $id = null): Transaction;

    /**
     * @param Carbon $date
     * @param TransactionType $transactionType
     * @param Card $card
     * @param string $cardNumber
     * @param float $amount
     * @param Batch $batch
     * @return Transaction
     * @throws \Exception
     */
    public function create(Carbon $date, TransactionType $transactionType, Card $card, string $cardNumber, float $amount, Batch $batch): Transaction;
}