<?php

namespace App\Repositories\Contracts;

use App\Models\Merchant\Merchant;

interface MerchantRepository
{
    /**
     * @param string $name
     * @param int $id
     * @return Merchant
     */
    public function make(string $name, int $id): Merchant;

    /**
     * @param string $type
     * @param int $id
     * @return Merchant
     * @throws \Exception
     */
    public function create(string $type, int $id): Merchant;
}