<?php

namespace App\Repositories\Contracts;

use App\Models\Card\Card;

interface CardRepository
{
    /**
     * @param string $type
     * @param int|null $id
     * @return Card
     */
    public function make(string $type, ?int $id = null): Card;

    /**
     * @param string $type
     * @return Card
     * @throws \Exception
     */
    public function create(string $type): Card;
}