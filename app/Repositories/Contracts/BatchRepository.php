<?php

namespace App\Repositories\Contracts;

use App\Models\Batch\Batch;
use App\Models\Merchant\Merchant;
use Carbon\Carbon;

interface BatchRepository
{
    /**
     * @param Carbon $date
     * @param int $refNumber
     * @param Merchant $merchant
     * @param int $id
     * @return Batch
     */
    public function make(Carbon $date, int $refNumber, Merchant $merchant, ?int $id = null): Batch;

    /**
     * @param Carbon $date
     * @param int $refNumber
     * @param Merchant $merchant
     * @return Batch
     * @throws \Exception
     */
    public function create(Carbon $date, int $refNumber, Merchant $merchant): Batch;
}