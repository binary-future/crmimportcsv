<?php


namespace App\Repositories;


use App\Models\Merchant\Merchant;
use App\Utils\Database\Contracts\Builder;
use App\Utils\Database\Contracts\Connection;

final class MerchantRepository implements \App\Repositories\Contracts\MerchantRepository
{
    private const FIELD_ID = 'id';
    private const FIELD_TYPE = 'name';

    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var Builder
     */
    private $builder;

    private $table = 'merchants';

    /**
     * MerchantRepository constructor.
     * @param Connection $connection
     * @param Builder $builder
     */
    public function __construct(Connection $connection, Builder $builder)
    {
        $builder->table($this->table);

        $this->connection = $connection;
        $this->builder = $builder;
    }

    /**
     * @param string $name
     * @param int $id
     * @return Merchant
     */
    public function make(string $name, int $id): Merchant
    {
        return new Merchant($id, $name);
    }

    /**
     * @param string $type
     * @param int $id
     * @return Merchant
     * @throws \Exception
     */
    public function create(string $type, int $id): Merchant
    {
        $bindings = [
            self::FIELD_ID => $id,
            self::FIELD_TYPE => $type
        ];
        $query = $this->builder->insert($bindings);

        if (! $this->connection->insert($query, $bindings)) {
            // TODO: create repository exception
            throw new \Exception('Merchant was not created');
        }

        return $this->make($type, $id);
    }


}