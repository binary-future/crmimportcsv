<?php


namespace App\Repositories;


use App\Models\Batch\Batch;
use App\Models\Card\Card;
use App\Models\Merchant\Merchant;
use App\Models\Transaction\Transaction;
use App\Models\Transaction\TransactionType;
use App\Utils\Database\Contracts\Builder;
use App\Utils\Database\Contracts\Connection;
use Carbon\Carbon;

final class TransactionRepository implements \App\Repositories\Contracts\TransactionRepository
{
    private const FIELD_ID = 'id';
    private const FIELD_DATE = 'date';
    private const FIELD_TYPE = 'type';
    private const FIELD_CARD_ID = 'card_id';
    private const FIELD_CARD_NUMBER = 'card_number';
    private const FIELD_AMOUNT = 'amount';
    private const FIELD_BATCH_ID = 'batch_id';

    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var Builder
     */
    private $builder;

    private $table = 'transactions';

    /**
     * BatchRepository constructor.
     * @param Connection $connection
     * @param Builder $builder
     */
    public function __construct(Connection $connection, Builder $builder)
    {
        $builder->table($this->table);

        $this->connection = $connection;
        $this->builder = $builder;
    }

    /**
     * @param Carbon $date
     * @param TransactionType $transactionType
     * @param Card $card
     * @param string $cardNumber
     * @param float $amount
     * @param Batch $batch
     * @param int $id
     * @return Transaction
     */
    public function make(
        Carbon $date,
        TransactionType $transactionType,
        Card $card,
        string $cardNumber,
        float $amount,
        Batch $batch,
        ?int $id = null
    ): Transaction
    {
        return new Transaction(
            $id, $date, $transactionType, $card, $cardNumber, $amount, $batch
        );
    }

    /**
     * @param Carbon $date
     * @param TransactionType $transactionType
     * @param Card $card
     * @param string $cardNumber
     * @param float $amount
     * @param Batch $batch
     * @return Transaction
     * @throws \Exception
     */
    public function create(
        Carbon $date,
        TransactionType $transactionType,
        Card $card,
        string $cardNumber,
        float $amount,
        Batch $batch
    ): Transaction
    {
        $bindings = [
            self::FIELD_DATE => $date,
            self::FIELD_TYPE => $transactionType->getType(),
            self::FIELD_CARD_ID => $card->getId(),
            self::FIELD_CARD_NUMBER => $cardNumber,
            self::FIELD_AMOUNT => $amount,
            self::FIELD_BATCH_ID => $batch->getId(),
        ];
        $query = $this->builder->insert($bindings);

        if (! $this->connection->insert($query, $bindings)) {
            // TODO: create repository exception
            throw new \Exception('Transaction was not created');
        }
        $transactionId = $this->connection->lastInsertId();

        return $this->make(
            $date,
            $transactionType,
            $card,
            $cardNumber,
            $amount,
            $batch,
            $transactionId
        );
    }


}