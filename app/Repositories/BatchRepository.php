<?php


namespace App\Repositories;


use App\Models\Batch\Batch;
use App\Models\Merchant\Merchant;
use App\Utils\Database\Contracts\Builder;
use App\Utils\Database\Contracts\Connection;
use Carbon\Carbon;

final class BatchRepository implements \App\Repositories\Contracts\BatchRepository
{
    private const FIELD_ID = 'id';
    private const FIELD_DATE = 'date';
    private const FIELD_REFERENCE_NUMBER = 'reference_number';
    private const FIELD_MERCHANT_ID = 'merchant_id';

    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var Builder
     */
    private $builder;

    private $table = 'batches';

    /**
     * BatchRepository constructor.
     * @param Connection $connection
     * @param Builder $builder
     */
    public function __construct(Connection $connection, Builder $builder)
    {
        $builder->table($this->table);

        $this->connection = $connection;
        $this->builder = $builder;
    }

    /**
     * @param Carbon $date
     * @param int $refNumber
     * @param Merchant $merchant
     * @param int $id
     * @return Batch
     */
    public function make(Carbon $date, int $refNumber, Merchant $merchant, ?int $id = null): Batch
    {
        return new Batch($id, $date, $refNumber, $merchant);
    }

    /**
     * @param Carbon $date
     * @param int $refNumber
     * @param Merchant $merchant
     * @return Batch
     * @throws \Exception
     */
    public function create(Carbon $date, int $refNumber, Merchant $merchant): Batch
    {
        $bindings = [
            self::FIELD_DATE => $date,
            self::FIELD_REFERENCE_NUMBER => $refNumber,
            self::FIELD_MERCHANT_ID => $merchant->getId()
        ];
        $query = $this->builder->insert($bindings);

        if (! $this->connection->insert($query, $bindings)) {
            // TODO: create repository exception
            throw new \Exception('Batch was not created');
        }
        $batchId = $this->connection->lastInsertId();

        return $this->make($date, $refNumber, $merchant, $batchId);
    }


}