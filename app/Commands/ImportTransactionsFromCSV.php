<?php


namespace App\Commands;


use App\Utils\CSVReader\Contracts\CSVReader;
use App\Utils\Import\ImportTransactions;
use App\Utils\Import\Transaction;

class ImportTransactionsFromCSV
{

    private $map = [
        'Transaction Date'          => Transaction::TRANSACTION_DATE,
        'Transaction Type'          => Transaction::TRANSACTION_TYPE,
        'Transaction Card Type'     => Transaction::TRANSACTION_CARD_TYPE,
        'Transaction Card Number'   => Transaction::TRANSACTION_CARD_NUMBER,
        'Transaction Amount'        => Transaction::TRANSACTION_AMOUNT,
        'Batch Date'                => Transaction::BATCH_DATE,
        'Batch Reference Number'    => Transaction::BATCH_REF_NUM,
        'Merchant ID'               => Transaction::MERCHANT_ID,
        'Merchant Name'             => Transaction::MERCHANT_NAME
    ];
    /**
     * @var ImportTransactions
     */
    private $importTransactions;
    /**
     * @var CSVReader
     */
    private $csvreader;

    /**
     * ImportTransactionsFromCSV constructor.
     * @param ImportTransactions $importTransactions
     * @param CSVReader $csvreader
     */
    public function __construct(
        ImportTransactions $importTransactions,
        CSVReader $csvreader
    )
    {
        $this->importTransactions = $importTransactions;
        $this->csvreader = $csvreader;
    }

    public function execute(string $filepath)
    {
        $columnsOrder = [];
        foreach ($this->readcsv($filepath) as $line) {
            if (empty($columnsOrder)) {
                $columnsOrder = $this->readHeader($line);
                if (count($columnsOrder) !== count($this->map)) {
                    throw new \RuntimeException(
                        'Amount of header columns is not identical to amount of map values'
                    );
                }
                continue;
            }

            try {
                $transaction = array_combine($columnsOrder, $line);
            } catch (\Throwable $ex) {
                throw new \RuntimeException(
                    'Amount of values is not identical to amount of columns',
                    $ex->getCode(),
                    $ex
                );
            }

            $transaction = Transaction::createFromArray($transaction);
            $this->importTransactions->import($transaction);
        }
    }


    private function readHeader(array $headers): array
    {
        $columnsOrder = [];
        array_map(function (string $header) use (&$columnsOrder) {
            $columnsOrder[] = $this->columnMapper($header);
        }, $headers);
        return $columnsOrder;
    }

    private function readcsv(string $filepath): ?\Generator
    {
        $header = null;
        foreach ($this->csvreader->read($filepath) as $line) {
            if(! $header) {
                yield $header = $line;
            } else {
                yield array_combine($header, $line);
            }
        }
    }

    private function columnMapper(string $header): string
    {
        return $this->map[$header];
    }
}