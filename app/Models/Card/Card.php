<?php


namespace App\Models\Card;


class Card
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var int|null
     */
    private $id;

    /**
     * Card constructor.
     * @param int|null $id
     * @param string $type
     */
    public function __construct(?int $id, string $type)
    {
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}