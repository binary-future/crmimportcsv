<?php


namespace App\Models\Transaction;


use App\Models\Batch\Batch;
use App\Models\Card\Card;
use Carbon\Carbon;

class Transaction
{
    /**
     * @var Carbon
     */
    private $date;
    /**
     * @var TransactionType
     */
    private $type;
    /**
     * @var Card
     */
    private $card;
    /**
     * @var string
     */
    private $cardNumber;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var Batch
     */
    private $batch;
    /**
     * @var int|null
     */
    private $id;

    /**
     * Transaction constructor.
     * @param int|null $id
     * @param Carbon $date
     * @param TransactionType $type
     * @param Card $card
     * @param string $cardNumber
     * @param float $amount
     * @param Batch $batch
     */
    public function __construct(
        ?int $id,
        Carbon $date,
        TransactionType $type,
        Card $card,
        string $cardNumber,
        float $amount,
        Batch $batch
    )
    {
        $this->id = $id;
        $this->date = $date;
        $this->type = $type;
        $this->card = $card;
        $this->cardNumber = $cardNumber;
        $this->amount = $amount;
        $this->batch = $batch;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }

    /**
     * @param Carbon $date
     */
    public function setDate(Carbon $date): void
    {
        $this->date = $date;
    }

    /**
     * @return TransactionType
     */
    public function getType(): TransactionType
    {
        return $this->type;
    }

    /**
     * @param TransactionType $type
     */
    public function setType(TransactionType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Card
     */
    public function getCard(): Card
    {
        return $this->card;
    }

    /**
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber(string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return Batch
     */
    public function getBatch(): Batch
    {
        return $this->batch;
    }

    /**
     * @param Batch $batch
     */
    public function setBatch(Batch $batch): void
    {
        $this->batch = $batch;
    }
}