<?php


namespace App\Models\Transaction;


final class TransactionType
{
    public const TYPE_SALE = 'sale';
    public const TYPE_REFUND = 'refund';

    /**
     * @var array
     */
    private $allowedTypes = [
        self::TYPE_SALE,
        self::TYPE_REFUND,
    ];

    /**
     * @var string
     */
    private $type;

    public function __construct(string $type)
    {
        $this->validateType($type);
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param string $type
     */
    private function validateType(string $type)
    {
        if (! in_array($type, $this->allowedTypes, true)) {
            throw new \InvalidArgumentException(
                "Invalid transaction type ($type). " .
                'Only the next types allowed: ' . implode(', ', $this->allowedTypes)
            );
        }
    }
}