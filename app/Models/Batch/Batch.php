<?php


namespace App\Models\Batch;


use App\Models\Merchant\Merchant;
use Carbon\Carbon;

final class Batch
{
    public const FIELD_ID = 'id';
    public const FIELD_DATE = 'date';
    public const FIELD_REFERENCE_NUMBER = 'reference_number';
    public const FIELD_MERCHANT_ID = 'merchant_id';

    /**
     * @var Carbon
     */
    private $date;
    /**
     * @var int
     */
    private $referenceNumber;
    /**
     * @var Merchant
     */
    private $merchant;
    /**
     * @var int|null
     */
    private $id;

    /**
     * Batch constructor.
     * @param int|null $id
     * @param Carbon $date
     * @param int $referenceNumber
     * @param Merchant $merchant
     */
    public function __construct(?int $id, Carbon $date, int $referenceNumber, Merchant $merchant)
    {
        $this->id = $id;
        $this->date = $date;
        $this->referenceNumber = $referenceNumber;
        $this->merchant = $merchant;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }

    /**
     * @param Carbon $date
     */
    public function setDate(Carbon $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getReferenceNumber(): int
    {
        return $this->referenceNumber;
    }

    /**
     * @param int $referenceNumber
     */
    public function setReferenceNumber(int $referenceNumber): void
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * @param Merchant $merchant
     */
    public function setMerchant(Merchant $merchant): void
    {
        $this->merchant = $merchant;
    }
}