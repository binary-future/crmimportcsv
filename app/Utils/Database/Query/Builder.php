<?php


namespace App\Utils\Database\Query;


use App\Utils\Database\Contracts\Connection;

class Builder implements \App\Utils\Database\Contracts\Builder
{
    /**
     * @var string
     */
    protected $table;
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * Builder constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $table
     * @return $this
     */
    public function table(string $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @param array[] $values
     * @return bool
     */
    public function insert(array $values): bool
    {
        if (empty($values)) {
            return true;
        }

        if (! is_array(reset($values))) {
            $values = [$values];
        } else {
            foreach ($values as $key => $value) {
                ksort($value);

                $values[$key] = $value;
            }
        }

        return $this->connection->insert(
            $this->prepareInsertQuery($values),
            $values
        );
    }

    /**
     * @return int
     */
    public function lastInsertId(): int
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @param array $values
     * @return string
     */
    protected function prepareInsertQuery(array $values)
    {
        $columns = implode(', ', array_keys($values));
        $parameters = implode(',', array_map(static function ($record) {
            return '(' . $this->parameterize($record) . ')';
        }, $values));

        return "INSERT INTO `$this->table` ($columns) VALUES $parameters";
    }

    /**
     * @param  array   $values
     * @return string
     */
    public function parameterize(array $values): string
    {
        return implode(', ', array_fill(0, count($values), '?'));
    }

}