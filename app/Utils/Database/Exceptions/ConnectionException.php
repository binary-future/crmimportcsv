<?php


namespace App\Utils\Database\Exceptions;


class ConnectionException extends \Exception
{
    /**
     * @param \Throwable $exception
     * @return ConnectionException
     */
    public static function unableConnect(\Throwable $exception): ConnectionException
    {
        return new self('Unable to connect.', $exception->getCode(), $exception);
    }
}