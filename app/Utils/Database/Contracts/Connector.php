<?php

namespace App\Utils\Database\Contracts;

use App\Utils\Database\Exceptions\ConnectionException;

interface Connector
{
    /**
     * @return \PDO
     * @throws ConnectionException
     */
    public function getConnection(): \PDO;
}