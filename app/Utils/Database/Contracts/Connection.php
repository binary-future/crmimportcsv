<?php

namespace App\Utils\Database\Contracts;

interface Connection
{
    public function beginTransaction();

    public function commit();

    public function rollback();

    /**
     * @param string $query
     * @param array $bindings
     * @return array
     */
    public function select(string $query, array $bindings = []): array;

    /**
     * @param string $query
     * @param array $bindings
     * @return bool
     */
    public function insert(string $query, array $bindings = []): bool;

    /**
     * @param string $query
     * @param array $bindings
     * @return int
     */
    public function update(string $query, array $bindings = []): int;

    /**
     * @param string $query
     * @param array $bindings
     * @return int
     */
    public function delete(string $query, array $bindings = []): int;

    /**
     * @return string
     */
    public function lastInsertId(): string;
}