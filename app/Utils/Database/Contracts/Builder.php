<?php

namespace App\Utils\Database\Contracts;

interface Builder
{
    /**
     * @param string $table
     * @return \App\Utils\Database\Query\Builder
     */
    public function table(string $table);

    /**
     * @param array[] $values
     * @return bool
     */
    public function insert(array $values): bool;

    /**
     * @return int
     */
    public function lastInsertId(): int;

    /**
     * @param  array   $values
     * @return string
     */
    public function parameterize(array $values): string;
}