<?php


namespace App\Utils\Database;


use App\Utils\Config\Contracts\Config;
use App\Utils\Database\Contracts\Connector as ConnectorInterface;
use App\Utils\Database\Exceptions\ConnectionException;

class Connector implements ConnectorInterface
{
    private const CONFIG_FILE = 'database';
    private const CONFIG_SCHEMA = 'connection';

    /**
     * @var \PDO
     */
    private $connection;
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $dbname;
    /**
     * @var string
     */
    private $dbuser;
    /**
     * @var string
     */
    private $password;

    /**
     * Connector constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $config = $config->get(self::CONFIG_FILE)[self::CONFIG_SCHEMA];
        $this->host = $config['host'] ?? 'localhost';
        $this->dbname = $config['dbname'];
        $this->dbuser = $config['dbuser'];
        $this->password = $config['password'];
    }

    /**
     * @param string $host
     * @param string $dbname
     * @return string
     */
    private function prepareDSN(string $host, string $dbname): string
    {
        return "mysql:host=$host;dbname=$dbname";
    }

    /**
     * @param string $host
     * @param string $dbname
     * @param string $dbuser
     * @param string $password
     * @return \PDO
     */
    private function openConnection(
        string $host,
        string $dbname,
        string $dbuser,
        string $password
    ): \PDO
    {
        $dsn = $this->prepareDSN($host, $dbname);
        return new \PDO($dsn, $dbuser, $password);
    }

    /**
     * @return \PDO
     * @throws ConnectionException
     */
    public function getConnection(): \PDO
    {
        if ($this->connection === null) {
            try {
                $this->connection = $this->openConnection(
                    $this->host,
                    $this->dbname,
                    $this->dbuser,
                    $this->password
                );
            } catch (\Throwable $exception) {
                throw ConnectionException::unableConnect($exception);
            }
        }
        return $this->connection;
    }
}