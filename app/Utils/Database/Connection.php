<?php


namespace App\Utils\Database;


use App\Utils\Database\Contracts\Connector as ConnectorInterface;
use App\Utils\Database\Exceptions\ConnectionException;

class Connection implements \App\Utils\Database\Contracts\Connection
{
    /**
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * MysqlAdapter constructor.
     * @param ConnectorInterface $connector
     * @throws ConnectionException
     */
    public function __construct(ConnectorInterface $connector)
    {
        $this->connection = $connector->getConnection();
    }

    public function beginTransaction()
    {
        $this->connection->beginTransaction();
    }

    public function commit()
    {
        $this->connection->commit();
    }

    public function rollback()
    {
        $this->connection->rollBack();
    }

    /**
     * @param \PDOStatement $statement
     * @param array $bindings
     */
    protected function bindValues(\PDOStatement $statement, array $bindings)
    {
        foreach ($bindings as $key => $value) {
            $statement->bindValue(
                is_string($key) ? $key : $key + 1,
                $value,
                is_int($value) || is_float($value)? \PDO::PARAM_INT : \PDO::PARAM_STR
            );
        }
    }

    /**
     * @param array $bindings
     * @return array
     */
    protected function prepareBindings(array $bindings): array
    {
        foreach ($bindings as $key => $value) {
            if ($value instanceof \DateTimeInterface) {
                $bindings[$key] = $value->format($this->dateFormat);
            } elseif (is_bool($value)) {
                $bindings[$key] = (int) $value;
            }
        }
        return $bindings;
    }

    /**
     * @param string $query
     * @param array $bindings
     * @return bool|\PDOStatement
     */
    protected function statement(string $query, array $bindings = [])
    {
        $statement = $this->connection->prepare($query);

        $this->bindValues($statement, $this->prepareBindings($bindings));

        return $statement;
    }

    /**
     * @param string $query
     * @param array $bindings
     * @return array
     */
    public function select(string $query, array $bindings = []): array
    {
        $statement = $this->statement($query, $bindings);

        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * @param string $query
     * @param array $bindings
     * @return bool
     */
    public function insert(string $query, array $bindings = []): bool
    {
        $statement = $this->statement($query, $bindings);

        return $statement->execute();
    }

    /**
     * @param string $query
     * @param array $bindings
     * @return int
     */
    public function update(string $query, array $bindings = []): int
    {
        return $this->affectingStatement($query, $bindings);
    }

    /**
     * @param string $query
     * @param array $bindings
     * @return int
     */
    public function delete(string $query, array $bindings = []): int
    {
        return $this->affectingStatement($query, $bindings);
    }

    /**
     * @return string
     */
    public function lastInsertId(): string
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @param string $query
     * @param array $bindings
     * @return int
     */
    protected function affectingStatement(string $query, array $bindings = []): int
    {
        $statement = $this->statement($query, $bindings);

        $statement->execute();

        return $statement->rowCount();
    }
}