<?php

namespace App\Utils\CSVReader\Contracts;

interface CSVReader
{
    /**
     * @param string $filepath
     * @return \Generator|null
     */
    public function read(string $filepath): ?\Generator;
}