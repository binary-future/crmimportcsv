<?php


namespace App\Utils\CSVReader;


use App\Utils\CSVReader\Contracts\CSVReader as CSVReaderInterface;

class CSVReader implements CSVReaderInterface
{
    /**
     * @param string $filepath
     * @return \Generator|null
     */
    public function read(string $filepath): ?\Generator
    {
        if (($handle = fopen($filepath, 'rb')) !== false) {
            while (($row = fgetcsv($handle)) !== false) {
                yield $row;
            }
            fclose($handle);
        }
    }
}