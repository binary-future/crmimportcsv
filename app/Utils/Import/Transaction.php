<?php


namespace App\Utils\Import;


class Transaction
{
    public const MERCHANT_ID             = 'mid'; // digits only, up to 18 digits
    public const MERCHANT_NAME           = 'dba'; // string, max length - 100
    public const BATCH_DATE              = 'batch_date'; // YYYY-MM-DD
    public const BATCH_REF_NUM           = 'batch_ref_num'; // digits only, up to 24 digits
    public const TRANSACTION_DATE        = 'trans_date'; // YYYY-MM-DD
    public const TRANSACTION_TYPE        = 'trans_type'; // string, max length - 20
    public const TRANSACTION_CARD_TYPE   = 'trans_card_type'; // string, max length - 2, possible values - VI/MC/AX and so on
    public const TRANSACTION_CARD_NUMBER = 'trans_card_num'; // string, max length - 20
    public const TRANSACTION_AMOUNT      = 'trans_amount'; // amount, negative values are possible

    /**
     * @var int
     */
    private $merchantId;
    /**
     * @var string
     */
    private $merchantName;
    /**
     * @var \DateTime
     */
    private $batchDate;
    /**
     * @var string
     */
    private $batchNumber;
    /**
     * @var \DateTime
     */
    private $transactionDate;
    /**
     * @var string
     */
    private $transactionType;
    /**
     * @var string
     */
    private $transactionCardType;
    /**
     * @var string
     */
    private $transactionCardNumber;
    /**
     * @var float
     */
    private $transactionAmount;

    public function __construct(
        int $merchantId,
        string $merchantName,
        string $batchDate,
        string $batchNumber,
        string $transactionDate,
        string $transactionType,
        string $transactionCardType,
        string $transactionCardNumber,
        float $transactionAmount
    )
    {
        $this->merchantId = $merchantId;
        $this->merchantName = $merchantName;
        $this->batchDate = new \DateTime($batchDate);
        $this->batchNumber = $batchNumber;
        $this->transactionDate = new \DateTime($transactionDate);
        $this->transactionType = $transactionType;
        $this->transactionCardType = $transactionCardType;
        $this->transactionCardNumber = $transactionCardNumber;
        $this->transactionAmount = $transactionAmount;
    }

    public static function createFromArray(array $data): Transaction
    {
        return new self(
            $data[self::MERCHANT_ID],
            $data[self::MERCHANT_NAME],
            $data[self::BATCH_DATE],
            $data[self::BATCH_REF_NUM],
            $data[self::TRANSACTION_DATE],
            $data[self::TRANSACTION_TYPE],
            $data[self::TRANSACTION_CARD_TYPE],
            $data[self::TRANSACTION_CARD_NUMBER],
            $data[self::TRANSACTION_AMOUNT],
        );
    }

    /**
     * @return int
     */
    public function getMerchantId(): int
    {
        return $this->merchantId;
    }

    /**
     * @return string
     */
    public function getMerchantName(): string
    {
        return $this->merchantName;
    }

    /**
     * @return \DateTime
     */
    public function getBatchDate(): \DateTime
    {
        return $this->batchDate;
    }

    /**
     * @return string
     */
    public function getBatchNumber(): string
    {
        return $this->batchNumber;
    }

    /**
     * @return \DateTime
     */
    public function getTransactionDate(): \DateTime
    {
        return $this->transactionDate;
    }

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    /**
     * @return string
     */
    public function getTransactionCardType(): string
    {
        return $this->transactionCardType;
    }

    /**
     * @return string
     */
    public function getTransactionCardNumber(): string
    {
        return $this->transactionCardNumber;
    }

    /**
     * @return float
     */
    public function getTransactionAmount(): float
    {
        return $this->transactionAmount;
    }
}