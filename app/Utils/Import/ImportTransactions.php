<?php


namespace App\Utils\Import;


use App\Utils\Database\Contracts\Builder;
use App\Utils\Database\Contracts\Connection;

class ImportTransactions
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(Connection $connection, Builder $builder)
    {
        $this->connection = $connection;
        $this->builder = $builder;
    }

    public function import(Transaction $transaction)
    {
        try {
            $this->connection->beginTransaction();
            $this->insertCard($transaction);
            $this->insertMerchant($transaction);
            $merchant = $this->selectLatestMerchant($transaction);
            $this->insertBatch($transaction, $merchant['id']);
            $card = $this->selectCard($transaction);
            $batch = $this->selectBatch($transaction);
            $this->insertTransaction($transaction, $card['id'], $batch['id']);
            $this->connection->commit();
        } catch (\Throwable $exception) {
            // TODO: move log to specific class
            echo $exception->getMessage();
            $this->connection->rollback();
        }
    }

    private function insertCard(Transaction $transaction)
    {
        $table = 'cards';
        $values = ['type' => $transaction->getTransactionCardType()];

        $this->insert($table, $values, true);
    }

    private function insertMerchant(Transaction $transaction)
    {
        $table = 'merchants';
        $values = [
            'name' => $transaction->getMerchantName(),
            'merchant_id' => $transaction->getMerchantId(),
        ];

        $this->insert($table, $values, true);
    }

    private function selectLatestMerchant(Transaction $transaction)
    {
        $table = 'merchants';

        $result = $this->select($table, ['id'], [
            'merchant_id' => $transaction->getMerchantId()
        ]);

        return end($result);
    }

    private function insertBatch(Transaction $transaction, int $merchantId)
    {
        $table = 'batches';
        $values = [
            'date' => $transaction->getBatchDate(),
            'reference_number' => $transaction->getBatchNumber(),
            'merchant_id' => $merchantId,
        ];

        $this->insert($table, $values, true);
    }

    private function selectCard(Transaction $transaction)
    {
        $table = 'cards';

        $result = $this->select($table, ['id'], [
            'type' => $transaction->getTransactionCardType()
        ]);

        return end($result);
    }

    private function selectBatch(Transaction $transaction)
    {
        $table = 'batches';

        $result = $this->select($table, ['id'], [
            'date' => $transaction->getBatchDate(),
            'reference_number' => $transaction->getBatchNumber(),
        ]);

        return end($result);
    }

    private function insertTransaction(Transaction $transaction, int $cardId, int $batchId)
    {
        $table = 'transactions';
        $values = [
            'date' => $transaction->getTransactionDate(),
            'type' => $transaction->getTransactionType(),
            'card_id' => $cardId,
            'card_number' => $transaction->getTransactionCardNumber(),
            'amount' => $transaction->getTransactionAmount(),
            'batch_id' => $batchId,
        ];

        $this->insert($table, $values);
    }

    private function insert(string $table, array $values, bool $ignore = false)
    {
        $columns = array_keys($values);
        $paramerized = $this->builder->parameterize($values);

        $sygnature = $ignore ? 'IGNORE' : '';

        $sql = "INSERT $sygnature INTO `$table` "
            . "({$this->prepareFields($columns)}) "
            . "VALUES ($paramerized);";

        $this->connection->insert($sql, array_values($values));
    }

    private function select(
        string $table,
        array $columns = ['*'],
        array $wheres = []
    ): array
    {
        $values = array_values($wheres);

        $columnsLine = implode(',', $columns);
        $sql = "SELECT $columnsLine FROM `$table` ";

        if(! empty($wheres)) {
            $whereSql = '';
            foreach ($wheres as $column => $value) {
                $parameterized = $this->builder->parameterize([$value]);
                if ($whereSql) {
                    $whereSql .= 'AND ';
                }
                $whereSql .= "$column=$parameterized ";
            }
            $sql .= "WHERE $whereSql";
        }

        return $this->connection->select($sql, array_values($values));
    }

    private function prepareFields(array $columns)
    {
        return implode(',', $columns);
    }
}