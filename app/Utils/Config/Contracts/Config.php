<?php

namespace App\Utils\Config\Contracts;

interface Config
{
    /**
     * @param string $path
     * @return array
     */
    public function get(string $path): array;
}