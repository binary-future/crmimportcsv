<?php


namespace App\Utils\Config;


use App\Utils\Config\Contracts\Config as ConfigInterface;
use Psr\Container\ContainerInterface;

class Config implements ConfigInterface
{
    private const CONFIG_PATH = '/config';

    /**
     * @var string basePath
     */
    private $basePath;

    /**
     * Config constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->basePath = $container->get('basePath');
    }

    /**
     * @param string $path
     * @return array
     */
    public function get(string $path): array
    {
        $path = $this->preparePath($path);
        if (! file_exists($path)) {
            throw new \InvalidArgumentException("File doesn't exist by path $path");
        }

        return include $path;
    }

    /**
     * @param string $path
     * @return string
     */
    private function preparePath(string $path): string
    {
        return $this->basePath . self::CONFIG_PATH . '/' . $this->sanitizePath($path) . '.php';
    }

    /**
     * @param string $path
     * @return string
     */
    private function sanitizePath(string $path): string
    {
        return trim($path, '/ ');
    }
}