CREATE SCHEMA IF NOT EXISTS iriscrm COLLATE latin1_general_ci;

CREATE TABLE IF NOT EXISTS cards
(
    type VARCHAR(10) NOT NULL,
    id SMALLINT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    
    CONSTRAINT cards_type_unique_index UNIQUE (type)
);

CREATE TABLE IF NOT EXISTS merchants
(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) DEFAULT '' NOT NULL,
    merchant_id VARCHAR(30) NOT NULL,

    CONSTRAINT merchants_merchant_id_unique_index
        UNIQUE (merchant_id)
);

CREATE TABLE IF NOT EXISTS batches
(
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    date DATE NOT NULL,
    reference_number VARCHAR(100) NOT NULL,
    merchant_id BIGINT UNSIGNED NOT NULL,

    CONSTRAINT batches_reference_number_unique_index
        UNIQUE (reference_number),
    CONSTRAINT batches_merchants_id_fk
        FOREIGN KEY (merchant_id) REFERENCES merchants (id)
            ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS transactions
(
    date DATE NOT NULL,
    type VARCHAR(50) NOT NULL,
    card_id SMALLINT(5) UNSIGNED NOT NULL,
    card_number VARCHAR(30) NOT NULL,
    amount DOUBLE NOT NULL,
    batch_id INT UNSIGNED NOT NULL,
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    
    CONSTRAINT transactions_batches_id_fk
        FOREIGN KEY (batch_id) REFERENCES batches (id)
            ON UPDATE CASCADE ON DELETE CASCADE ,
            
    CONSTRAINT transactions_cards_id_fk
        FOREIGN KEY (card_id) REFERENCES cards (id)
            ON UPDATE CASCADE ON DELETE CASCADE
);

